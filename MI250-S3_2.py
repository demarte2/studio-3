import random
import sys
qval = 0
question =""
user_resp=""
resp=""

spring = ["Fantastic! Do you enjoy the flowers?", "Me too! Do you celebrate Easter with your family?", "What do you like about spring?"]
spring_resp = ["Great! I love those too.", "I do too!", "That is great! I enjoy the breeze."]
summer = ["Do you like the beach?", "What is your favorite summer activity?", "Do you celebrate 4th of July?"]
summer_resp = ["Great! Me too!", "Awesome! Mine is sailing.", "I love the firework shows."]
winter = ["That is great! Do you want to build a snowman?", "Mine as well! What do you like to do during Winter?", "Sweet! Do you like New Years or Christmas?"]
winter_resp = ["I love making igloos too!", "Great! I like to snowboard.", "I love New Years!"]
fall = ["That is great! Do you want to make a leaf pile?", "Mine too! What do you like about fall?", "Oh really? Do you like Halloween?"]
autumn = fall
fall_resp = ["I love the colors of the leaves!", "I love apple cider and donuts.", "I love the Spooky season too."]
List_season = [summer,winter,fall,autumn,spring]

print("Welcome to Mother Earth!")
print("What is your favorite season?")
while True:                                    # Keep going forever
    resp = input("> ").lower().strip()              # Ask the user for input, and save it in the "input" variable
    if "goodbye" in resp:                       # Check and see if the user said goodbye
        print("Goodbye!")                          #   If they did, say goodbye back
        sys.exit(0)
    if resp == "fall" or resp == "autumn":
        qval = random.randint(0,len(fall)-1)
        question = fall[qval]
        user_resp = input(question)
        print(fall_resp[qval])
    if resp == "winter":
        qval = random.randint(0,len(winter)-1)
        question = winter[qval]
    if resp == "spring":
        qval = random.randint(0,len(spring)-1)
        question = spring[qval]
    if resp == "summer":
        qval = random.randint(0,len(summer)-1)
        question = summer[qval]
    if (resp != "summer") or (resp != "fall") or (resp != "winter") or (resp != "spring") or (resp!="autumn"):
        print("invalid season b o i ")
