import random
import sys

responses = [
  ("fall" or "autumn",
     ["That is great! What is another favorite?",
      "Mine too! What do you like about fall?",
      "Oh really? Do you like Halloween?"
      ]),
  ("winter",
     ["That is great! What is another favorite?",
      "Mine as well! What do you like to do during Winter?",
      "Sweet! Do you like New Years or Christmas?"
      ]),
  ("spring",
     ["Fantastic! Do you enjoy the flowers?",
      "Me too! Do you celebrate Easter with your family?",
      "What do you like about spring?"
      ]),
  ("summer",
     ["Do you like the beach?",
      "What is your favorite summer activity?",
      "Do you celebrate 4th of July?"
      ]),
  ("Yes",
     ["Why do you like it?",
      "Nice!",
      "What do like about "
      ]),
    ]

print("Welcome to Mother Earth!")
print("What is your favorite season?")

while True:                                    # Keep going forever
  resp = input("> ").lower()              # Ask the user for input, and save it in the "input" variable
  if "goodbye" in resp:                       # Check and see if the user said goodbye
    print("Goodbye!")                          #   If they did, say goodbye back
    sys.exit(0)                                #   and then exit (end) the program
                                               # If we get here, then the user didn't say goodbye.  So let's figure out how to response
  for response in responses:                   # Look through all of the possible reponses listed above
    if response[0] in resp:                   #   Check if what the user said is the prompt we are looking at
      print(random.choice(response[1]))        #     If the prompt matches, then choose a random response and print it out for the user
      break                                    #     If the prompt matches, then stop looing for more prompts so we don't print out more than one response
